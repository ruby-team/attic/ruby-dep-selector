ruby-dep-selector (1.0.3-3) UNRELEASED; urgency=medium

  [ Cédric Boutillier ]
  * Bump debhelper compatibility level to 9
  * Use https:// in Vcs-* fields
  * Use https:// in Vcs-* fields
  * Bump Standards-Version to 3.9.7 (no changes needed)
  * Run wrap-and-sort on packaging files
  * Use new default gem2deb Rakefile to run the tests

  [ Utkarsh Gupta ]
  * Add salsa-ci.yml

 -- Utkarsh Gupta <guptautkarsh2102@gmail.com>  Tue, 13 Aug 2019 03:56:59 +0530

ruby-dep-selector (1.0.3-2) unstable; urgency=medium

  * Patch: search for dep_gecode.so in vendorarchdir

 -- Hleb Valoshka <375gnu@gmail.com>  Wed, 22 Jul 2015 17:42:23 +0300

ruby-dep-selector (1.0.3-1) unstable; urgency=medium

  * New upstream release
  * Fix debian/copyright
  * Add ruby-ffi as runtime dependency
  * Add ruby-rspec, rake, ruby-ffi and ruby-solve to build dependencies
  * Increase Standards-Version to 3.9.6 (no changes)
  * Add myself to uploaders
  * Remove not needed debian/source/options
  * Add rake task to run tests during build
  * Install no binaries
  * Patches:
    - do not require dep-selector-libgecode
    - port to libgecode >= 4.0 (by Cédric Boutillier)
    - do not set rpath
    - make package solve >= 2.0 compatible
    - make tests RSpec3 compatible

 -- Hleb Valoshka <375gnu@gmail.com>  Wed, 08 Jul 2015 11:07:34 +0300

ruby-dep-selector (0.0.8-2) unstable; urgency=low

  * Team upload
  * debian/control:
    + remove obsolete DM-Upload-Allowed flag
    + use canonical URI in Vcs-* fields
    + Bump Standards-Version to 3.9.5 (no changes needed)
    + add minimal version 4.0 for libgecode-dev
    + build for all versions supported by gem2deb (currently 1.9.1 and 2.0)
    + improve a little bit the description
    + add a debian/source/patch-header file
  * debian/copyright: use DEP5 copyright-format/1.0 official URL
  * Port to libgecode >= 4.0 (Closes: #710104)
    - Restart class has been renamed to RBS which needs an Engine (chose DFS)
    - instances of RBS need a Search::Cutoff (arbitraly chose Geometric)
    - variable selection are now functions and need brackets
    - statistics about memory are not available in gecode (by default)

 -- Cédric Boutillier <boutil@debian.org>  Tue, 26 Nov 2013 15:26:33 +0100

ruby-dep-selector (0.0.8-1) unstable; urgency=low

  * Initial release

 -- Tollef Fog Heen <tfheen@debian.org>  Sun, 10 Jun 2012 21:07:26 +0200
